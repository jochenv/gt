import argparse
import os
import toml
import codecs
import sys
from git import Repo, Git, Submodule
import pathlib
import logging
import subprocess

_local_config_name = ".config.toml"
_git_repo: Repo = None
_logger = None
_magic_config_repo_root = ".config.repo.root"
_config_repo_root = None


class Package:
  def __init__(self, name, url = None, branch = None, commit = None, tag = None):
    self.name = name
    self.url = url
    self.branch = branch
    self.commit = commit
    self.tag = tag
    self.dependencies = []
    super().__init__()

  def getCheckoutTo(self):
    if self.tag != None:
      return self.tag
    if self.commit != None:
      return self.commit
    if self.branch != None:
      return self.branch
    return None

  def setDict(self, d):
    for k,v in d.items():
      self.__dict__[k] = v

  def asDict(self, ignoreNone = False):
    result = dict()
    for k,v in self.__dict__.items():
      if not ignoreNone or v != None:
        result[k] = v
    return result

  def __str__(self):
    return str(self.__dict__)

  def __repr__(self):
    return str(self)

class Dependency:
  def __init__(self, config_file, package):
    self.config_file = config_file
    self.package = package
    super().__init__()
  def __repr__(self):
    return self.config_file

def _init_logger(level):
  # Inits the logger
  global _logger
  _logger = logging.getLogger("gt")
  _logger.setLevel(logging.DEBUG)
  #handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=20, backupCount=5)
  ch = logging.StreamHandler()
  ch.setLevel(level)
  formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
  ch.setFormatter(formatter)
  _logger.addHandler(ch)

def logger():
  global _logger
  return _logger

## internalhelpers

def _get_config_repo_root(path):
  global _magic_config_repo_root, _config_repo_root
  if _config_repo_root == None:
    path = os.path.abspath(path)
    if os.path.isfile(path):
      path,_ = os.path.split(path)
    while True:
      if os.path.exists(os.path.join(path, _magic_config_repo_root)):
        _config_repo_root = path
        break
      newpath,_ = os.path.split(path) # moving one directory up
      if newpath == path:
        break # newpath == system root
      path = newpath
  if _config_repo_root == None:
    logger().critical("No root directory found for the config repository")
    sys.exit(1)
  return _config_repo_root  

def _check_config(configpath):
  # Check if the config file exists.
  # If not the user should do the init command which also manipulates the .gitignore
  global _logger
  path = configpath
  if not os.path.exists(path):
    _logger.critical(f"Config file {path} does not exist.")
    sys.exit(0)

def _read_config(configpath):
  # Read in the config and convert the map into Dependency objects
  _check_config(configpath)
  config_repo_path = _get_config_repo_root(configpath)

  p = Package("new")
  with codecs.open(configpath, 'r', 'utf8') as f:
    data = f.read()
    data = toml.loads(data)
    p.setDict(data)
    for i in range(0, len(p.dependencies)):
      # read dependend config and replace dependecy
      dep_cfg = os.path.join(config_repo_path, p.dependencies[i])
      dep_pck = _read_config(dep_cfg)
      p.dependencies[i] = Dependency(p.dependencies[i], dep_pck)
  return p

def _write_config(package, configpath):
  # Converts the Dependency objects into a map and writes the config
  _check_config(configpath)
  towrite = package.asDict()
  del towrite['dependencies']
  towrite['dependencies'] = []
  for dep in package.dependencies:
    towrite['dependencies'].append(dep.config_file)
  with codecs.open(configpath, 'w', 'utf8') as f:
    f.write(toml.dumps(towrite))

def _read_local_config():
  global _local_config_name
  _check_config(_local_config_name)
  data = None
  with codecs.open(_local_config_name, 'r', 'utf8') as f:
    data = f.read()
    data = toml.loads(data)
  return data
  
def _write_local_config(data):
  global _local_config_name
  with codecs.open(_local_config_name, 'w', 'utf8') as f:
    f.write(toml.dumps(data))


def _flatten_dependencies(package):
  result = []
  for dep in package.dependencies:
    pck = dep.package
    result.append(pck)
    result.extend(_flatten_dependencies(pck))
  return result

def _get_all_packages(configpath):
  p = _read_config(configpath)
  all_pcks = _flatten_dependencies(p)
  managed = dict()
  for pck in all_pcks:
    if not pck.url in managed:
      managed[pck.url] = pck
    else:
      #todo: manage version conflicts
      pass
  result = list(managed.values())
  result.append(p) #original package must be there
  return result


def _path_from_url(url):
  _,path = os.path.split(url)
  path,_ = os.path.splitext(path)
  return path

## actions
def config_init(name, url, branch, commit, tag, file):
  if not file:
    file = f"{name}.toml"
  if os.path.exists(file):
    logger().critical(f"{file} already exists, exiting...")
    sys.exit(1)
  p = Package(name, url, branch, commit, tag)
  logger().info(f"Creating config for {name}")
  with codecs.open(file, "w", "utf-8") as f:
    pass # create config file
  _write_config(p, file)
  
def add_dependency(config_path, dependend_path):
  config_path = os.path.abspath(config_path)
  repo_root = _get_config_repo_root(config_path)
  dep_path = os.path.relpath(os.path.abspath(dependend_path), repo_root)

  package = _read_config(config_path)
  dep_pck = _read_config(os.path.abspath(dependend_path))
  logger().info(f"Adding {dep_pck.name} as a dependency to {package.name}")
  package.dependencies.append(Dependency(dep_path, dep_pck))
  _write_config(package, config_path)
  
def clone(config_path, directory):
  global _local_config_name
  logger().info("Creating new repository")
  _write_local_config({'config_path': config_path})
  pcks = _get_all_packages(config_path)
  subprocess.run(["git","init"])
  for pck in pcks:
    subprocess.run(["git","submodule","add",pck.url])
    if pck.getCheckoutTo() != None:
      subprocess.run(["git","checkout",pck.getCheckoutTo()], cwd=_path_from_url(pck.url))

  subprocess.run(["git","add",_local_config_name])
  subprocess.run(["git","commit", "-m", "'initial commit'"])

def update():
  global _local_config_name
  logger().info("Updating repository and dependencies")
  cfg = _read_local_config()
  config_path = cfg['config_path']
  pcks = _get_all_packages(config_path)
  subprocess.run(["git","submodule","update","--remote","--merge"])
  for pck in pcks:
    path = _path_from_url(pck.url)
    if not os.path.exists(path):
      subprocess.run(["git","submodule","add",pck.url])
    subprocess.run(["git", "add", path])
    if pck.getCheckoutTo() != None:
      subprocess.run(["git","checkout",pck.getCheckoutTo()], cwd=path)

  subprocess.run(["git","commit", "-m", "'updated"])

def status():
  subprocess.run(["git","submodule","status"])


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  subparsers = parser.add_subparsers(dest='action')
  parser.add_argument('-v', '--verbose', action='store_true', help="Increase verbosity")
  parser.add_argument('-q', '--quiet', action='store_true', help="No logging to stdout (will overwrite -v)")

  parser_init = subparsers.add_parser("config-init", help="Creates a config file")
  parser_init.add_argument('name', type=str, help='The url of the dependent repository')
  parser_init.add_argument('url', type=str, default=None, help='The url of the dependent repository')
  parser_init.add_argument('-b', '--branch', type=str, default='master', help='Track specific branch')
  parser_init.add_argument('-c', '--commit', type=str, default='HEAD', help='Lock to specific commit (sha265)')
  parser_init.add_argument('-t', '--tag', type=str, default=None, help='Lock to specific tag')
  parser_init.add_argument('-f', '--file', type=str, default=None, help='The file to write to, if not set <name>.toml will be generated')

  parser_add = subparsers.add_parser("config-add", help="Add a dependency")
  parser_add.add_argument('configpath', type=str, help='The path of of the config.')
  parser_add.add_argument('dependendpath', type=str, help='The path of of the dependent repository\'s config.')

  parser_rm = subparsers.add_parser("config-rm", help="Remove a dependency")
  parser_rm.add_argument('configpath', type=str, help='The path of of the config.')
  parser_rm.add_argument('dependendpath', type=str, help='The path of of the dependent repository\'s config.')


  parser_clone = subparsers.add_parser("clone", help="Clones a repository from a given config")
  parser_clone.add_argument("configpath", type=str, help="The path of the config")

  parser_pull = subparsers.add_parser("update", help="Fetches and merges the main repository and all dependencies")
  parser_pull.add_argument('--only-submodules', action='store_true', help='Don\'t update the main repository')

  parser_status = subparsers.add_parser("status", help="Prints the status of all dependencies")

  args = parser.parse_args()

  # _open_repo(os.getcwd())

  log_level = logging.INFO
  if(args.verbose):
    log_level = logging.DEBUG
  if(args.quiet):
    log_level = logging.FATAL

  _init_logger(log_level)

  if args.action == "config-init":
    config_init(args.name, args.url, args.branch, args.commit, args.tag, args.file)
  if args.action == "config-add":
    add_dependency(args.configpath, args.dependendpath)
  if args.action == "clone":
    clone(args.configpath, os.getcwd())
  if args.action == "update":
    update()
  if args.action == "status":
    status()